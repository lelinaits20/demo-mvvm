package com.example.demomvvm.ui.auth

import com.example.demomvvm.data.entities.User

interface AuthListener {

    fun onSuccess(loginResonse: User)
    fun onStarted()
    fun onError(msg:String)
}