package com.example.demomvvm.ui.home

import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.demomvvm.R
import com.example.demomvvm.data.AppDatabase
import com.example.demomvvm.data.entities.Person
import com.example.demomvvm.data.network.MyApi
import com.example.demomvvm.data.network.NetworkConnectionInterceptor
import com.example.demomvvm.data.repository.UserRepository
import com.example.demomvvm.databinding.ActivityHomeBinding
import com.example.demomvvm.ui.base.BaseActivity
import com.example.demomvvm.utils.toast
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.coroutines.launch

class HomeActivity : BaseActivity(),HomeListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val networkConnectionInterceptor= NetworkConnectionInterceptor(this)

        val appDb = AppDatabase(this)
        val api = MyApi(networkConnectionInterceptor)
        val repository = UserRepository(api, appDb)
        val factory = HomeViewModerFactory(repository)

        val binding: ActivityHomeBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_home)
        val viewModel = ViewModelProviders.of(this, factory).get(HomeViewModel::class.java)
        binding.homeviewmoder = viewModel
        viewModel.homeListener = this


    }

    override fun onSuccess() {
        toast("data saved successfully")
    }

    override fun onError(message:String) {
        toast(message)
    }

}
