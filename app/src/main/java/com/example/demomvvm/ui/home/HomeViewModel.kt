package com.example.demomvvm.ui.home

import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModel
import com.example.demomvvm.data.entities.Person
import com.example.demomvvm.data.repository.UserRepository
import com.example.demomvvm.utils.Corotines

class HomeViewModel(private val userRepository: UserRepository) :ViewModel() {
    var firstname:String?=null
    var lastName:String?=null
    var address:String?=null

    var homeListener:HomeListener?=null

     fun onSaveButonClicked(view:View){
         Log.e("people","save button is clicked")
        if (firstname.isNullOrEmpty()||lastName.isNullOrEmpty()||address.isNullOrEmpty()){
            homeListener?.onError("fields must be null")

            return
        }
         else{
            Corotines.main {
                userRepository.savePeople(Person(0,firstname!!,lastName!!,address!!))
                homeListener?.onSuccess()
            }
        }




    }


    fun onShowButtonIsClicked(view: View){
        Log.e("people","show button is clicked")

        Corotines.main {
            val peopleList=userRepository.getAllPeople()

            for (i in peopleList){
                Log.e("people","First name :${i.firstName}")
                Log.e("people","Last name :${i.lastName}")
                Log.e("people","Address name :${i.address}")
            }
        }
    }



}