package com.example.demomvvm.ui.auth

import android.view.View
import androidx.lifecycle.ViewModel
import com.example.demomvvm.data.repository.UserRepository
import com.example.demomvvm.utils.ApiException
import com.example.demomvvm.utils.Corotines
import com.example.demomvvm.utils.NoInternetException

class AuthViewModer(private val repository: UserRepository) :ViewModel() {
    var email:String?=null
    var password:String?=null
    var authListener:AuthListener?=null

    fun getLoggedInUser()=repository.getUser()

    fun onLoginIsClicked(view:View){
        if (email.isNullOrEmpty()||password.isNullOrEmpty()){
            authListener?.onError("Invalid Email or Password")
            return
        }
        Corotines.main {
            authListener?.onStarted()
           try {
               val authResponse=repository.userLogin(email!!,password!!)
               authResponse.user?.let {
                   authListener?.onSuccess(it)
                   repository.saveUser(it)
                   return@main
               }

               authListener?.onError(authResponse.message!!)

           }catch (e:ApiException){
               authListener?.onError(e.message!!)
           }catch (e :NoInternetException){
               authListener?.onError(e.message!!)

           }

        }
    }
}