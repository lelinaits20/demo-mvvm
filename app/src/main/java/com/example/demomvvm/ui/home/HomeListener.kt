package com.example.demomvvm.ui.home

import com.example.demomvvm.data.entities.Person

interface HomeListener {
    fun onSuccess()
    fun onError(message:String)

}