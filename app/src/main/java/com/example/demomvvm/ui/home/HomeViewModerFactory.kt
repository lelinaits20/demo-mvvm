package com.example.demomvvm.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.demomvvm.data.repository.UserRepository

@Suppress("UNCHECKED_CAST")
class HomeViewModerFactory(private val repository: UserRepository) :ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeViewModel(repository) as T
    }

}