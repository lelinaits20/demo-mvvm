package com.example.demomvvm.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.demomvvm.R
import com.example.demomvvm.data.AppDatabase
import com.example.demomvvm.data.entities.User
import com.example.demomvvm.data.network.MyApi
import com.example.demomvvm.data.network.NetworkConnectionInterceptor
import com.example.demomvvm.data.repository.UserRepository
import com.example.demomvvm.databinding.ActivityLoginBinding
import com.example.demomvvm.ui.home.HomeActivity
import com.example.demomvvm.utils.toast
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(),AuthListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val networkConnectionInterceptor=NetworkConnectionInterceptor(this)
        val api=MyApi(networkConnectionInterceptor)
        val appDatabase=AppDatabase(this)
        val repository=UserRepository(api,appDatabase)
        val factory=AuthViewModerFactory(repository)
        val binding: ActivityLoginBinding =DataBindingUtil.setContentView(this,R.layout.activity_login)
        val viewModel=ViewModelProviders.of(this,factory).get(AuthViewModer::class.java)
        binding.viewmodel=viewModel
        viewModel.authListener=this

        viewModel.getLoggedInUser().observe(this, Observer {user ->
            if (user!=null){
                Intent(this,HomeActivity::class.java).also {
                    it.flags=Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(it)
                }
            }
        })
    }



    override fun onSuccess(loginResonse: User) {
//        Intent(this,HomeActivity::class.java).also {
//            it.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
//            startActivity(it)
//        }

        progressBar.visibility= View.GONE
        toast("${loginResonse.name} is Logged In")
    }

    override fun onStarted() {
        progressBar.visibility= View.VISIBLE
    }

    override fun onError(msg: String) {
        progressBar.visibility= View.GONE
        toast(msg)
    }



}
