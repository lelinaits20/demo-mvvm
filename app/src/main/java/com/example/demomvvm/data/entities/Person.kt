package com.example.demomvvm.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity
data class Person(
      @PrimaryKey(autoGenerate = true)
     val id:Int,
    val firstName:String,
    val lastName:String,
    val address:String
) {
}