package com.example.demomvvm.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.demomvvm.data.entities.Person

@Dao
interface PersonDao {

    @Insert
    suspend fun addPerson(person:Person)

    @Query("SELECT * FROM Person")
    suspend fun getAllPerson():List<Person>
}