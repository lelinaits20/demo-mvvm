package com.example.demomvvm.data.network.responses

import com.example.demomvvm.data.entities.User


class AuthResponse(
    val isSuccessful:Boolean?,
    val message:String?,
    val user:User?
)