package com.example.demomvvm.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.demomvvm.data.AppDatabase
import com.example.demomvvm.data.entities.Person
import com.example.demomvvm.data.entities.User
import com.example.demomvvm.data.network.MyApi
import com.example.demomvvm.data.network.SafeApiRequest
import com.example.demomvvm.data.network.responses.AuthResponse
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserRepository(
    private val api: MyApi,
    private val appDatabase: AppDatabase
) :SafeApiRequest(){

    suspend fun userLogin(email:String,password:String):AuthResponse{
        return apiRequest { api.userLogin(email,password) }
    }

    suspend fun saveUser(user:User)=appDatabase.getUserDao().upsert(user)

    suspend fun savePeople(person: Person)=appDatabase.getPersonDao().addPerson(person)
    suspend fun getAllPeople()=appDatabase.getPersonDao().getAllPerson()

    fun getUser()=appDatabase.getUserDao().getUser()
}