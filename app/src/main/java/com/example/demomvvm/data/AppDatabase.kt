package com.example.demomvvm.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.demomvvm.data.entities.Person
import com.example.demomvvm.data.entities.User

@Database(
    entities = [User::class,Person::class],
    version = 4
)
abstract class AppDatabase :RoomDatabase() {

   abstract fun getUserDao():UserDao
   abstract fun getPersonDao():PersonDao

    companion object{
        @Volatile
        private var instance:AppDatabase?=null
        private val LOCK=Any()

        operator fun invoke(context: Context)= instance?: synchronized(LOCK){
            instance?:buildDataBase(context).also {
                instance=it
            }
        }

        private fun buildDataBase(context: Context)=
            Room.databaseBuilder(context.applicationContext,AppDatabase::class.java,"MyDatabase.db")
                .build()
    }
}