package com.example.demomvvm.utils

import android.content.Context
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import es.dmoral.toasty.Toasty

fun Context.toast(message:String){
    Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
}
fun ProgressBar.show(){
    visibility=View.VISIBLE
}

fun ProgressBar.hide(){
    visibility=View.GONE
}